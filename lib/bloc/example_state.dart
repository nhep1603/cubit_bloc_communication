part of 'example_bloc.dart';

class ExampleState extends Equatable {
  const ExampleState({required this.value});
  final int value;
  @override
  List<Object> get props => [value];
}

