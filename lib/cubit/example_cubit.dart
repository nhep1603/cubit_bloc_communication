import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cubit_bloc_communication/bloc/example_bloc.dart';
import 'package:equatable/equatable.dart';

part 'example_state.dart';

class ExampleCubit extends Cubit<ExampleState> {
  ExampleCubit({required this.bloc}) : super(ExampleState(value: 0)){
    otherBlocSubscription = bloc.stream.listen((event) {
      add();
     });
  }
  final ExampleBloc bloc;
  late final StreamSubscription otherBlocSubscription;

  add() => emit(ExampleState(value: state.value + 2));

  @override
  Future<void> close() {
    otherBlocSubscription.cancel();
    return super.close();
  }
}
