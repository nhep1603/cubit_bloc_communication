import 'package:cubit_bloc_communication/bloc/example_bloc.dart';
import 'package:cubit_bloc_communication/cubit/example_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app_bloc_observer.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = AppBocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ExampleBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MultiBlocProvider(
          providers: [
            // BlocProvider(
            //   create: (context) => ExampleBloc()..add(ExampleEvent()),
            // ),
            BlocProvider(
              create: (context) =>
                  ExampleCubit(bloc: BlocProvider.of<ExampleBloc>(context)),
            ),
          ],
          child: Home(),
        ),
      ),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(50.0),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'cubit : ${context.select((ExampleCubit cubit) => cubit.state.value)}',
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                      fontSize: 60.0,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'bloc : ${context.select((ExampleBloc bloc) => bloc.state.value)}',
                    style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                      fontSize: 60.0,
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () =>
                        context.read<ExampleBloc>().add(ExampleEvent()),
                    child: Text(
                      'add',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
